<?php
/**
 * Implements hook_requirements().
 *
 * Verifies that PHP CURL library is installed on the server
 */
function uc_cardpay_requirements($phase) {
  $t = get_t();

  $has_curl = function_exists('curl_init');

  $requirements['uc_cardpay_curl'] = array(
    'title' => $t('cURL'),
    'value' => $has_curl ? $t('Enabled') : $t('Not found'),
  );
  if (!$has_curl) {
    $requirements['uc_cardpay_curl']['severity'] = REQUIREMENT_ERROR;
    $requirements['uc_cardpay_curl']['description'] = $t("CardPay Payment Gateway Module requires the PHP <a href='!curl_url'>cURL</a> library.", array('!curl_url' => 'http://php.net/manual/en/curl.setup.php'));
  }

  // check whether mcrypt extension is installed
  $extension = get_loaded_extensions();
  if (array_search('mcrypt', $extension) === FALSE) {
    $requirements['uc_cardpay_mcrypt'] = array(
      'title' => $t('Mcrypt'),
      'value' => $t('Not found'),
      'severity' => REQUIREMENT_ERROR,
      'description' => $t("CardPay Payment Gateway Module requires the PHP <a href='!mcrypt_url'>Mcrypt</a> library.", array('!mcrypt_url' => 'http://php.net/manual/en/book.mcrypt.php')),
    );
  }
  return $requirements;
}

/**
 * Implements hook_uninstall().
 *
 * Uninstalls all variables set by the uc_mypaymentgateway module.
 */
function uc_cardpay_uninstall() {
  // Delete related variables all at once.
  db_query("DELETE FROM {variable} WHERE name LIKE 'uc_cardpay_%%'");
}